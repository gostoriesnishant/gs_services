<?php include "databaseHandler.php";?>
<?php include "errorHandlerForAPI.php";?>
<?php include "validation.php";?>
<?php include "user.php";?>
<?php
$errorHandlerForAPI = new errorHandlerForAPI;
$databaseHandler = new databaseHandler;
$validation = new validation;
$user = new user;
$jsonTemplateForAPI = new JsonTemplateForAPI;

if(isset($_POST[globalKeys::$emailKey]))
{
	if(isset($_POST[globalKeys::$promoCodeKey]))
	{
		if($_POST[globalKeys::$emailKey] == "" || $_POST[globalKeys::$emailKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5005);
		}
		if($_POST[globalKeys::$promoCodeKey] == "" || $_POST[globalKeys::$promoCodeKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5014);
		}
		
		$email = $_POST[globalKeys::$emailKey];
		$promoCode = $_POST[globalKeys::$promoCodeKey];
		
		$con = $databaseHandler->getConnection();
		
		$resultForEmail = $validation->checkEmailExists($con, $email);
		if(!$resultForEmail)
		{
			$errorHandlerForAPI->getErrorJson(5012);
		}
		
		// Check for promo code exists
		$resultForPromoCode = $validation->checkPromoCodeExists($con, $promoCode);
		if($resultForPromoCode)
		{
			$errorHandlerForAPI->getErrorJson(5018);
		}
		
		// Check for self promo code
		$resultForSelfPromoCode = $validation->checkSelfPromoCode($con, $email, $promoCode);
		if($resultForSelfPromoCode)
		{
			$errorHandlerForAPI->getErrorJson(5015);
		}
		
		// Check for promo code is genrated by same UDID
		$resultForPromoCodeForSameUDID = $validation->checkPromoCodeForSameUDID($con, $email, $promoCode);
		if($resultForPromoCodeForSameUDID)
		{
			$errorHandlerForAPI->getErrorJson(5016);
		}
		
		// Check for promo code is already applied
		$isPromoApplied = '1';
		
		$resultForPromoCodeAlreadyApplied = $validation->checkPromoCodeAlreadyApplied($con, $email, $isPromoApplied);
		if($resultForPromoCodeAlreadyApplied)
		{
			$errorHandlerForAPI->getErrorJson(5017);
		}
		
		// Update Consumer balance 
		$user->getUserBalanceByEmailID($con, $email);
		$user->balance = $user->balance + globalKeys::$promoBalanceForConsumer;
		$user->setUserBalanceByEmailID($con, $user->balance, $email);
		$user->setUserPromoCodeAppliedByEmailID($con, $email, $promoCode);
		
		// Update Producer balance
		$userNew = new User;
		$userNew->getUserBalanceByPromoCode($con, $promoCode);
		$userNew->balance = $userNew->balance + globalKeys::$promoBalanceForProducer;
		$userNew->setUserBalanceByPromoCode($con, $userNew->balance, $promoCode);
		
		
		$userBalance = $jsonTemplateForAPI->getUserbalanceForSuccessPromoCodeTemplate();
		
		$userBalance[globalKeys::$successKey][globalKeys::$balanceKey] = $user->balance;
		
		print_r(json_encode($userBalance));
		die;
	}
	else
	{
		$errorHandlerForAPI->getErrorJson(5013);
	}
}
else
{
	$errorHandlerForAPI->getErrorJson(5003);
}
?>