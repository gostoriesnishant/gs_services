<?php include "databaseHandler.php";?>
<?php include "errorHandlerForAPI.php";?>
<?php include "validation.php";?>
<?php include "user.php";?>
<?php include "story.php";?>
<?php
$errorHandlerForAPI = new errorHandlerForAPI;
$databaseHandler = new databaseHandler;
$validation = new validation;
$user = new user;
$story = new story;
$jsonTemplateForAPI = new JsonTemplateForAPI;

if(isset($_POST[globalKeys::$emailKey]))
{
	if(isset($_POST[globalKeys::$storyIDKey]))
	{
		if($_POST[globalKeys::$emailKey] == "" || $_POST[globalKeys::$emailKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5005);
		}
		if($_POST[globalKeys::$storyIDKey] == "" || $_POST[globalKeys::$storyIDKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5021);
		}
		
		$email = $_POST[globalKeys::$emailKey];
		$storyKey = $_POST[globalKeys::$storyIDKey];
		
		$con = $databaseHandler->getConnection();
		
		$resultForEmail = $validation->checkEmailExists($con, $email);
		if(!$resultForEmail)
		{
			$errorHandlerForAPI->getErrorJson(5012);
		}
		
		$resultForStoryID = $validation->checkStoryIDExists($con, $storyKey);
		if(!$resultForStoryID)
		{
			$errorHandlerForAPI->getErrorJson(5022);
		}
		
		// Check for Stroy purchased
		$resultForStoryPurchased = $story->checkForStoryPurchased($con, $email, $storyKey);
		$storyPurchased = $jsonTemplateForAPI->getIsPurchasedStoryTemplate();
		
		if($resultForStoryPurchased)
		{
			$storyPurchased[globalKeys::$successKey][globalKeys::$isPurchasedKey] = globalKeys::$isPurchasedValueYesKey;
		}
		else
		{
			$storyPurchased[globalKeys::$successKey][globalKeys::$isPurchasedKey] = globalKeys::$isPurchasedValueNoKey;
		}
		
		print_r(json_encode($storyPurchased));
		die;
	}
	else
	{
		$errorHandlerForAPI->getErrorJson(5020);
	}
}
else
{
	$errorHandlerForAPI->getErrorJson(5003);
}
?>