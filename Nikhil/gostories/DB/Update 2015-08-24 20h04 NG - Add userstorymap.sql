CREATE TABLE `userstorymap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `storyID` int(11) NOT NULL,
  `createdTimeStamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_userstorymap_userID` (`userID`),
  KEY `fk_story_userstorymap_storyID` (`storyID`),
  CONSTRAINT `fk_user_userstorymap_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_story_userstorymap_storyID` FOREIGN KEY (`storyID`) REFERENCES `story` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
