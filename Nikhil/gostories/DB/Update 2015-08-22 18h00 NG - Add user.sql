CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `udid` varchar(100) NOT NULL,
  `phoneNumber` varchar(100) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `selfPromoCode` varchar(100) NOT NULL,
  `isPromoApplied` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
