<?php include "globalKeys.php";?>
<?php
class JsonTemplateForAPI
{
	public function getErrorTemplate()
	{
		$errorTemplate = array(globalKeys::$errorKey=>array(globalKeys::$errorCodeKey => "", globalKeys::$errorMessageKey => "")); 
		return $errorTemplate;
	}
	public function getUserCreationTemplate()
	{
		$userCreationTemplate = array(globalKeys::$successKey=>array(globalKeys::$selfPromoCodeKey => "", globalKeys::$balanceKey => "")); 
		return $userCreationTemplate;
	}
	public function getUserbalanceForErrorTemplate()
	{
		$userBalanceTemplate = array(globalKeys::$errorKey=>array(globalKeys::$balanceKey => "")); 
		return $userBalanceTemplate;
	}
	public function getUserbalanceForSuccessTemplate()
	{
		$userBalanceTemplate = array(globalKeys::$successKey=>""); 
		return $userBalanceTemplate;
	}
	public function getUserbalanceForSuccessPromoCodeTemplate()
	{
		$userBalanceTemplate = array(globalKeys::$successKey=>array(globalKeys::$balanceKey => "")); 
		return $userBalanceTemplate;
	}
	
	public function getIsPurchasedStoryTemplate()
	{
		$userBalanceTemplate = array(globalKeys::$successKey=>array(globalKeys::$isPurchasedKey => "")); 
		return $userBalanceTemplate;
	}
	public function getPurchasedStoryForSuccessTemplate()
	{
		$userBalanceTemplate = array(globalKeys::$successKey=>""); 
		return $userBalanceTemplate;
	}
}
?>