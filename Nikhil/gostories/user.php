<?php
class user
{
	public $id;
	public $email;
	public $udid;
	public $phoneNumber;
	public $balance;
	public $selfPromoCode;
	public $isPromoApplied;
	
	public function createUser($con)
	{
		$createdTimeStamp = date('Y-m-d H:i:s');
		
		$sql = "INSERT INTO user VALUES('','".$this->email."','".$this->udid."','".$this->phoneNumber."','".$this->balance."','".$this->selfPromoCode."','".$this->isPromoApplied."','".$createdTimeStamp."','')";

		$queryResult = mysql_query($sql,$con);
		$this->id = mysql_insert_id();
		
		return $queryResult;
	}
	
	public function getUserDetailsByEmailID($con, $email)
	{
		//die($email);
		$sql = "select id, email, udid, phoneNumber, balance, selfPromoCode, isPromoApplied from user where email = '$email'";
		$queryResult = mysql_query($sql,$con);
		
		while($userDetails = mysql_fetch_assoc($queryResult))
		{
			$this->id = $userDetails["id"];
			$this->email = $userDetails["email"];
			$this->udid = $userDetails["udid"];
			$this->phoneNumber = $userDetails["phoneNumber"];
			$this->balance = $userDetails["balance"];
			$this->selfPromoCode = $userDetails["selfPromoCode"];
			$this->isPromoApplied = $userDetails["isPromoApplied"];
		}
	}
	
	public function getUserBalanceByEmailID($con, $email)
	{
		//die($email);
		$sql = "select balance from user where email = '$email'";
		$queryResult = mysql_query($sql,$con);
		
		while($userDetails = mysql_fetch_assoc($queryResult))
		{
			$this->balance = $userDetails["balance"];
		}
	}
	
	public function setUserBalanceByEmailID($con, $balance, $email)
	{
		$sql = "UPDATE user set balance = '$balance' where email = '$email'";

		$queryResult = mysql_query($sql,$con);		
		return $queryResult;
	}
	
	public function setUserPromoCodeAppliedByEmailID($con, $email, $appliedPromoCode)
	{
		$sql = "UPDATE user set isPromoApplied = '1', appliedPromoCode = '$appliedPromoCode' where email = '$email'";

		$queryResult = mysql_query($sql,$con);		
		return $queryResult;
	}
	
	
	public function getUserBalanceByPromoCode($con, $promoCode)
	{
		//die($email);
		$sql = "select balance from user where selfPromoCode = '$promoCode'";
		$queryResult = mysql_query($sql,$con);
		
		while($userDetails = mysql_fetch_assoc($queryResult))
		{
			$this->balance = $userDetails["balance"];
		}
	}
	
	public function setUserBalanceByPromoCode($con, $balance, $promoCode)
	{
		$sql = "UPDATE user set balance = '$balance' where selfPromoCode = '$promoCode'";

		$queryResult = mysql_query($sql,$con);		
		return $queryResult;
	}
	
	function genratePromoCode($length = 5) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';

		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, strlen($characters) - 1)];
		}

		return $string;
	}


}
?>