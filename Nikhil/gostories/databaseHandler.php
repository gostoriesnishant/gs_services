<?php
class databaseHandler
{
	private $ipAddress = "192.168.1.109";
	private $username = "admin";
	private $password = "admin";
	private $dbname = "gostories";
	
	public function getConnection()
	{
		$errorHandlerForAPI = new ErrorHandlerForAPI;
		try
		{
			$con = mysql_connect($this->ipAddress,$this->username,$this->password);
			
			if(!$con)
			{
				$errorHandlerForAPI->getErrorJson(5001);
			}
			else
			{
				$dbcon = mysql_select_db($this->dbname,$con);
				if($dbcon)
				{
					return $con;
				}
				else
				{
					$errorHandlerForAPI = new ErrorHandlerForAPI;
					$errorHandlerForAPI->getErrorJson(5002);
				}
				mysql_close();
			}
		}
		catch(Exception $e)
		{
			$errorHandlerForAPI->getErrorJson();
		}
	}
}
?>