<?php include "databaseHandler.php";?>
<?php include "errorHandlerForAPI.php";?>
<?php include "validation.php";?>
<?php include "user.php";?>
<?php include "story.php";?>
<?php
$errorHandlerForAPI = new errorHandlerForAPI;
$databaseHandler = new databaseHandler;
$validation = new validation;
$user = new user;
$story = new story;
$jsonTemplateForAPI = new JsonTemplateForAPI;

if(isset($_POST[globalKeys::$emailKey]))
{
	if(isset($_POST[globalKeys::$storyIDKey]))
	{
		if($_POST[globalKeys::$emailKey] == "" || $_POST[globalKeys::$emailKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5005);
		}
		if($_POST[globalKeys::$storyIDKey] == "" || $_POST[globalKeys::$storyIDKey] == null)
		{
			$errorHandlerForAPI->getErrorJson(5021);
		}
		
		$email = $_POST[globalKeys::$emailKey];
		$storyKey = $_POST[globalKeys::$storyIDKey];
		
		$con = $databaseHandler->getConnection();
		
		$resultForEmail = $validation->checkEmailExists($con, $email);
		if(!$resultForEmail)
		{
			$errorHandlerForAPI->getErrorJson(5012);
		}
		
		$resultForStoryKey = $validation->checkStoryIDExists($con, $storyKey);
		if(!$resultForStoryKey)
		{
			$errorHandlerForAPI->getErrorJson(5022);
		}
		
		// Check for Stroy purchased
		$resultForStoryPurchased = $story->checkForStoryPurchased($con, $email, $storyKey);
		if($resultForStoryPurchased)
		{
			$errorHandlerForAPI->getErrorJson(5023);
		}
		
		// Get userID from Email
		$userID = '';
		$user->getUserDetailsByEmailID($con, $email);
		$userID = $user->id;
		
		// Get storyID from Stroy Key
		$storyID = $story->getStoryIDByStoryKey($con, $storyKey);
		
		// insert for Story purchased
		$story->setStoryPurchase($con, $userID, $storyID);
		$storyPurchasedForSuccess = $jsonTemplateForAPI->getPurchasedStoryForSuccessTemplate();
		
		print_r(json_encode($storyPurchasedForSuccess));
		die;
	}
	else
	{
		$errorHandlerForAPI->getErrorJson(5020);
	}
}
else
{
	$errorHandlerForAPI->getErrorJson(5003);
}
?>