<?php  error_reporting(0); ?>
<?php
class globalKeys
{
	public static $errorKey = "Error";
	public static $errorCodeKey = "ErrorCode";
	public static $errorCodePrefixKey = "API-Error : ";
	public static $errorMessageKey = "ErrorMessage";
	
	public static $successKey = "Success";
	
	public static $emailKey = "RegisteredEmailId";
	public static $phoneNumberKey = "RegisteredPhoneNumber";
	public static $uDIDKey = "RegisteredUDId";
	public static $selfPromoCodeKey = "SELF_PROMO_CODE";
	public static $promoCodeKey = "FRIENDS_REFERRAL_CODE";
	public static $balanceKey = "WALLET_BALANCE";
	public static $prevBalanceKey = "PrevBalance";
	public static $newBalanceKey = "NewBalance";
	
	public static $storyIDKey = "StoryID";
	public static $isPurchasedKey = "IsPurchased";
	public static $isPurchasedValueYesKey = "Yes";
	public static $isPurchasedValueNoKey = "No";
	
	public static $welcomeBalance = "2000";
	public static $promoBalanceForConsumer = "40";
	public static $promoBalanceForProducer = "60";
}
?>