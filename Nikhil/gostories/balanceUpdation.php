<?php include "databaseHandler.php";?>
<?php include "errorHandlerForAPI.php";?>
<?php include "validation.php";?>
<?php include "user.php";?>
<?php
$errorHandlerForAPI = new errorHandlerForAPI;
$databaseHandler = new databaseHandler;
$validation = new validation;
$user = new user;
$jsonTemplateForAPI = new JsonTemplateForAPI;

if(isset($_POST[globalKeys::$emailKey]))
{
	if(isset($_POST[globalKeys::$prevBalanceKey]))
	{
		if(isset($_POST[globalKeys::$newBalanceKey]))
		{
			if($_POST[globalKeys::$emailKey] == "" || $_POST[globalKeys::$emailKey] == null)
			{
				$errorHandlerForAPI->getErrorJson(5005);
			}
			if($_POST[globalKeys::$prevBalanceKey] == "" || $_POST[globalKeys::$prevBalanceKey] == null)
			{
				$errorHandlerForAPI->getErrorJson(5010);
			}
			if($_POST[globalKeys::$newBalanceKey] == "" || $_POST[globalKeys::$newBalanceKey] == null)
			{
				$errorHandlerForAPI->getErrorJson(5011);
			}
			
			$email = $_POST[globalKeys::$emailKey];
			$prevBalance = $_POST[globalKeys::$prevBalanceKey];
			$newBalance = $_POST[globalKeys::$newBalanceKey];
			
			$con = $databaseHandler->getConnection();
			
			$resultForEmail = $validation->checkEmailExists($con, $email);
			if(!$resultForEmail)
			{
				$errorHandlerForAPI->getErrorJson(5012);
			}
			
			$user->getUserBalanceByEmailID($con, $email);
			
			$balance = $user->balance;
			
			if($balance == $prevBalance)
			{
				// Previous Input balance is same as DB Balance - Update Balance into DB
				
				$resultForBalanceUpdate = $user->setUserBalanceByEmailID($con, $newBalance, $email);
				$userBalance = $jsonTemplateForAPI->getUserbalanceForSuccessTemplate();
				
				print_r(json_encode($userBalance));
				die;
			}
			else
			{
				// Previous Input balance is not same as DB Balance - Return DB balance
				
				$userBalance = $jsonTemplateForAPI->getUserbalanceForErrorTemplate();
		
				$userBalance[globalKeys::$errorKey][globalKeys::$balanceKey] = $balance;
				
				print_r(json_encode($userBalance));
				die;
			}
		}
		else
		{
			$errorHandlerForAPI->getErrorJson(5009);
		}
	}
	else
	{
		$errorHandlerForAPI->getErrorJson(5008);
	}
}
else
{
	$errorHandlerForAPI->getErrorJson(5003);
}
?>