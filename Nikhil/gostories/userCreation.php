<?php include "databaseHandler.php";?>
<?php include "errorHandlerForAPI.php";?>
<?php include "validation.php";?>
<?php include "user.php";?>
<?php
$errorHandlerForAPI = new errorHandlerForAPI;
$databaseHandler = new databaseHandler;
$validation = new validation;
$user = new user;
$jsonTemplateForAPI = new JsonTemplateForAPI;

if(isset($_POST[globalKeys::$emailKey]))
{
	if(isset($_POST[globalKeys::$uDIDKey]))
	{
		if(isset($_POST[globalKeys::$phoneNumberKey]))
		{
			if($_POST[globalKeys::$emailKey] == "" || $_POST[globalKeys::$emailKey] == null)
			{
				$errorHandlerForAPI->getErrorJson(5005);
			}
			if($_POST[globalKeys::$uDIDKey] == "" || $_POST[globalKeys::$uDIDKey] == null)
			{
				$errorHandlerForAPI->getErrorJson(5006);
			}
			
			$email = $_POST[globalKeys::$emailKey];
			$uDID = $_POST[globalKeys::$uDIDKey];
			$phoneNumber = $_POST[globalKeys::$phoneNumberKey];
			
			
			$con = $databaseHandler->getConnection();
			
			$resultForEmail = $validation->checkEmailExists($con, $email);
			if($resultForEmail)
			{
				// User Already Exists
				
				$user->getUserDetailsByEmailID($con, $email);
			}
			else
			{
				// User Not Exists

				// check for UDID 
				$resultForUDID = $validation->checkUDIDExists($con, $uDID);
				
				if($resultForUDID)
				{
					// UDID is same - Create user with no welcome balance
					
					$user->email = $email;
					$user->udid = $uDID;
					$user->phoneNumber = $phoneNumber;
					$user->balance = "0";
					$user->selfPromoCode = $user->genratePromoCode();
					$user->isPromoApplied = "0"; // No
					
					$resultForUserCreation = $user->createUser($con);
					
					if($resultForUserCreation)
					{
						// Success 
					}
					else
					{
						$errorHandlerForAPI->getErrorJson(5007);
					}
				}
				else
				{
					// UDID is not same - Create user with welcome balance
					
					$user->email = $email;
					$user->udid = $uDID;
					$user->phoneNumber = $phoneNumber;
					$user->balance = globalKeys::$welcomeBalance;
					$user->selfPromoCode = $user->genratePromoCode();
					$user->isPromoApplied = "0"; // No
					
					$resultForUserCreation = $user->createUser($con);
					
					if($resultForUserCreation)
					{
						// Success 
					}
					else
					{
						$errorHandlerForAPI->getErrorJson(5007);
					}
				}
			}
			$userCreation = $jsonTemplateForAPI->getUserCreationTemplate();
			
			$userCreation[globalKeys::$successKey][globalKeys::$selfPromoCodeKey] = $user->selfPromoCode;
			$userCreation[globalKeys::$successKey][globalKeys::$balanceKey] = $user->balance;
			
			print_r(json_encode($userCreation));
			die;
		}
		else
		{
			$errorHandlerForAPI->getErrorJson(5019);
		}
	}
	else
	{
		$errorHandlerForAPI->getErrorJson(5004);
	}
}
else
{
	$errorHandlerForAPI->getErrorJson(5003);
}
?>