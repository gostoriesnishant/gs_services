<?php
/*************** Nishant Thite *****************/
/*********** Started : 12-Aug-2015  ************/

// Global vars
$INVALID_DATA = "INVALID_DATA";
$FREE_WELCOME_BALANCE = "100";

function getErrorJSON($errNumber, $errDetail) {
	$errArr = array("Error" => array( $errNumber => $errDetail."Contact GoStories support team with this error code.\nfriendz@gostories.co.in"));
	return json_encode($errArr);
}

$GivenEmailId = "GivenEmailId";
$GivenPhoneNumber = "GivenPhoneNumber";
$GivenUDId = "GivenUDId";

$JSON_Array = $INVALID_DATA ;
get_POST_JSON();
if(!(is_array($JSON_Array))){
	// Error-2 appears if registration request does not contain valid JSON inputs
		die(getErrorJSON("GSS Error-2", ""));
}

function get_POST_JSON(){
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$received_json = file_get_contents("php://input");
		$GLOBALS["JSON_Array"] = json_decode($received_json, true);
	}
	else{
		// Error-1 appears if HTTP request type is not POST
		die(getErrorJSON("GSS Error-1", ""));
	}
}

function printArray($array){
     foreach ($array as $key => $value){
        echo "$key => $value";
        if(is_array($value)){ //If $value is an array, print it as well!
            printArray($value);
        }  
    } 
}
$email_id = $phone_number = $udid = $SELF_PROMO_CODE = "";

$email = get_clean_input_from_post($GivenEmailId);
$phone_number = get_clean_input_from_post($GivenPhoneNumber);
$udid = get_clean_input_from_post($GivenUDId);

 //echo("emailR: {$email} \n");

function get_clean_input_from_post($param_name) {

	$data =  $GLOBALS["INVALID_DATA"];
	//echo "\n\n";
	if (isset($GLOBALS["JSON_Array"][$param_name])) {
		$data = $GLOBALS["JSON_Array"][$param_name];
	}
	
	//echo "\n\n";
  	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}

function create_self_promo_code($emailId, $userId){
	$userIdStr = "". $userId;
	return "GS" . substr($emailId, 0, 2) . "PC" . substr($userIdStr, 0, 2);
}

$servername = "localhost";
$dbname = "GoStoriesDB";
$username = "";
$password = "";

// Set PRODUCTION as 1 before commiting to LIVE environment
$PRODUCTION = 0;
if ($PRODUCTION == 1) {
	$username = "nishant8creo";
	$password = "nishant8creo";
}else{
	// Nishant's local host
	$username = "root";
	$password = "mysql"; 
}

$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
	// Error-3 appears if DataBase connection fails
	// Generate a unique id and display to user
	$errDetails = "GSS Error-3. Connection failed. ". $conn->connect_error ;
	//error_log($errDetails, 1, "NishanT@Creo-invent.com");
	die(getErrorJSON("GSS Error- 3", ""));
}

$newUserSql = "INSERT INTO GSUSERS ( EMAIL_ID, UDID, PHONE_NUMBER, WALLET_BALANCE, SELF_PROMO_CODE, APPLIED_PROMO_CODE) VALUES ('$email', '$udid', '$phone_number', 100, 'TBC','NO')";

if ($conn->query($newUserSql) === TRUE) {
	$userPromoCode = create_self_promo_code($email, $conn->insert_id);
	$successArr = array("Success" => array( "SELF_PROMO_CODE" => $userPromoCode, "WALLET_BALANCE" => $GLOBALS->$FREE_WELCOME_BALANCE ));
	$registrationSuccess = json_encode($successArr);
	echo ($registrationSuccess);
}else{
	// Error-4 appears if query execution fails
	// Generate a unique id and display to user
	$errDetails = "GSS Error-4. Query execution failed. ". $conn->error ;
	//error_log($errDetails, 1, "NishanT@Creo-invent.com");
	die(getErrorJSON("GSS Error- 3", ""));
}

$conn->close();

?>