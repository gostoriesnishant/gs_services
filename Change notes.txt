
/*************** Nishant Thite *****************/
/*********** Started : 12-Aug-2015  ************/
/******************* v0.1.3  *******************/

v0.0.1
- Basic php code to read POSTed JSON
=============================================

v0.0.2
- Parsing POSTed JSON and echo-ing the same
=============================================

v0.0.3
- Table set up through SQL
- Fetching and inserting data in db table
=============================================

v0.0.4
- Directory structure for SQL, PHP and JSON
- Added required POST JSON structure for new registration
=============================================

v0.0.5

- Added required JSON structure for ERRORS
- JSON response with SELF_PROMO_SODE and WALLET_BALANCE
=============================================

v0.0.6

- Created JSON structure by array to replace hard coding
- Key for default welcome WALLET_BALANCE
- Changes in connection and DB related error details to protect business information
=============================================

v0.0.7
- Added PRODUCTION environment DB credential separator.
=============================================

v0.1.0
- Added Nikhil’s APIs
=============================================

v0.1.2
- Changed some global keys’ value
=============================================

v0.1.3
- Masked some error messages for business privacy
=============================================

Common and imp git commands
(01). git init
(02). git add -all
(03). git commit -m “commit message”
(04). git remote add origin "https://MY_REMOTE_GIT_SERVER_PATH/MY_GIT_FILE.git"
(05). git push origin master
(06). git pull origin master

